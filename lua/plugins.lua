local fn = vim.fn
local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
if fn.empty(fn.glob(install_path)) > 0 then
  packer_bootstrap = fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
end

return require('packer').startup(function(use)
  -- My Plugins:
  
    use 'wbthomason/packer.nvim'    
    use 'Mofiqul/dracula.nvim'
    use { 'nvim-lualine/lualine.nvim',
    requires = { 'kyazdani42/nvim-web-devicons', opt = true } }
    use 'norcalli/nvim-colorizer.lua'
   -- Completion plugins. 
    use 'hrsh7th/nvim-cmp' 
    use 'hrsh7th/cmp-buffer' -- Buffer Completions Source. 
    use 'hrsh7th/cmp-path' -- Path completions Source. 
    use 'hrsh7th/cmp-cmdline' -- Cmdline completions Source. 
    use 'saadparwaiz1/cmp_luasnip' -- Snippet completions Source. 
    -- Snippets.
    use 'L3MON4D3/LuaSnip' -- Snippet engine. 

  if packer_bootstrap then
    require('packer').sync()
  end
end)
