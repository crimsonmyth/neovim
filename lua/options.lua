vim.opt.rnu = true
vim.opt.nu = true
vim.opt.tgc = true
vim.opt.cursorline = false
vim.o.backup = false
vim.bo.swapfile = false 
vim.opt.incsearch = true
vim.bo.expandtab = true
vim.bo.shiftwidth = 4
vim.bo.softtabstop = 4
vim.o.errorbells = false
vim.opt.scrolloff = 8
vim.opt.sidescrolloff =  8
vim.opt.clipboard = "unnamedplus"
vim.opt.hlsearch = true
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.cmd [[set iskeyword+=-]] --vim.cmd [[ foo ]] passes a viml string.

vim.cmd[[colorscheme dracula]]
