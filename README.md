# Neovim - Vim but More Scalable. 
[Neovim](https://www.neovim.io/) is a refactor, and sometimes redactor, in the tradition of Vim (which itself derives from Stevie). It is 
not a rewrite but a continuation and extension of [Vim](https://www.vim.org/). Many clones and derivatives exist, some very clever—but none 
are Vim. Neovim is built for users who want the good parts of Vim, and more. You can find Neovim's Source Code [Here](https://www.github.com/neovim/neovim/).

## Prerequisites.
- Neovim. I strongly recommend against the use of Neovim Nightly.
- [Nodejs](https://nodejs.org/en/).
- [Npm](https://www.npmjs.com/).
- Pip.
- [Xsel](https://github.com/kfish/xsel). 
- Neovim Python Support:
```bash
pip install pynvim
```
Neovim Node Support:
```bash
npm i -g neovim
```

## Plugins Installed: 
- dracula.nvim 
- popup.nvim
- lualine.nvim
- nvim-web-devicons  
- packer.nvim
- colorizer.nvim

## Download.
Clone the Repository With Git:
```bash
git clone https://gitlab.com/crimsonmyth/neovim.git ~/.config
```

Let's First Go To the Doctor, Open Nvim and Enter the following:
```bash
:checkhealth
```
You have now installed my Configuration of Neovim, If you encounter any errors. Do Report it as a GitLab Issue. 

<div align="center" id="madewithlua">
	
[![Lua](https://img.shields.io/badge/Made%20with%20Lua-blue.svg?style=for-the-badge&logo=lua)](#madewithlua)
	
</div>
